package metar

import (
	"reflect"
	"testing"
	"time"
)

func Test_metarToJson(t *testing.T) {
	type args struct {
		metar Metar
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{"First", args{metar: Metar{
			CodeType:        "METAR",
			ICAO:            "EDDH",
			ObservationTime: time.Date(1977, time.April, 11, 10, 10, 0, 0, time.UTC)}},
			[]byte("{\"code_type\":\"METAR\",\"icao\":\"EDDH\",\"observation_time\":\"1977-04-11T10:10:00Z\"}"), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := metarToJson(tt.args.metar)
			if (err != nil) != tt.wantErr {
				t.Errorf("metarToJson() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("metarToJson() got = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}
