package header

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	timeRegex     = "^([0123]{1}\\d{1}[012]{1}\\d{1}[012345]{1}\\d{1}Z)$"
	codeTypeRegex = "^(METAR|SPECI){1}$"

	metar CodeType = "METAR"
	speci CodeType = "SPECI"
)

// CodeType is one of "METAR" or "SPECI"
type CodeType string

// ICAO is the location-identifier
type ICAO string

// String returns the string value of the CodeType
func (c CodeType) String() string {
	return string(c)
}

// ParseCodeType returns the CodeType from rawMetarCode
func ParseCodeType(rawMetarCode string) (CodeType, error) {

	validCodePrefix := regexp.MustCompile(codeTypeRegex)
	if !validCodePrefix.MatchString(rawMetarCode) {
		return "", fmt.Errorf("Invalid Code Type %s ", rawMetarCode)
	}

	var result CodeType

	if strings.HasPrefix(rawMetarCode, metar.String()) {
		result = metar
	} else {
		result = speci
	}

	return result, nil
}

func ParseICAO(rawICAO string) (ICAO, error) {
	if len(rawICAO) != 4 {
		return "", fmt.Errorf("invalid ICAO: %s, length must be 4", rawICAO)
	}
	return ICAO(rawICAO), nil
}

// ParseObservationTime creates a time for raw time string in format hhmmssZ
func ParseObservationTime(rawTime string) (time.Time, error) {
	validTime := regexp.MustCompile(timeRegex)
	if !validTime.MatchString(rawTime) {
		return time.Time{}, fmt.Errorf("raw time is invalid, should be ddhhmmZ but is  %s", rawTime)
	}
	day, _ := strconv.Atoi(rawTime[0:2])
	hour, _ := strconv.Atoi(rawTime[2:4])
	minute, _ := strconv.Atoi(rawTime[4:6])

	now := time.Now().UTC()
	date := time.Date(now.Year(), now.Month(), day, hour, minute, 0, 0, time.UTC)
	return date, nil
}
