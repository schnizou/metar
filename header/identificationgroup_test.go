package header

import (
	"reflect"
	"testing"
	"time"
)

func Test_parseObservationTime(t *testing.T) {
	type args struct {
		rawTime string
	}
	var tests = []struct {
		name    string
		args    args
		want    time.Time
		wantErr bool
	}{
		{name: "Day 12 23:46", args: args{"122346Z"}, want: createObservationTime(12, 23, 46), wantErr: false},
		{name: "Day 39 23:46", args: args{"392346Z"}, want: createObservationTime(39, 23, 46), wantErr: false},
		{name: "Invalid", args: args{"39246Z"}, want: time.Time{}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseObservationTime(tt.args.rawTime)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseObservationTime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseObservationTime() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func createObservationTime(day, hour, minute int) time.Time {
	now := time.Now().UTC()
	myTime := time.Date(now.Year(), now.Month(), day, hour, minute, 0, 0, time.UTC)
	return myTime
}

func TestCodeName_String(t *testing.T) {
	tests := []struct {
		name string
		c    CodeType
		want string
	}{
		{"METAR", metar, "METAR"},
		{"SPECI", speci, "SPECI"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseCodeName(t *testing.T) {
	type args struct {
		rawMetar string
	}
	tests := []struct {
		name    string
		args    args
		want    CodeType
		wantErr bool
	}{
		{"METAR Prefix", args{"METAR"}, metar, false},
		{"SPECI Prefix", args{"SPECI"}, speci, false},
		{"Code is missing", args{""}, CodeType(""), true},
		{"Code is wrong", args{"Pimmel"}, CodeType(""), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseCodeType(tt.args.rawMetar)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseCodeType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ParseCodeType() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseICAO(t *testing.T) {
	type args struct {
		rawICAO string
	}
	tests := []struct {
		name    string
		args    args
		want    ICAO
		wantErr bool
	}{
		{"valid", args{"EDDH"}, ICAO("EDDH"), false},
		{"to short", args{"EDD"}, ICAO(""), true},
		{"to long", args{"EDDer"}, ICAO(""), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseICAO(tt.args.rawICAO)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseICAO() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ParseICAO() got = %v, want %v", got, tt.want)
			}
		})
	}
}
