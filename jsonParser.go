package metar

import (
	"encoding/json"
)

func metarToJson(metar Metar) ([]byte, error) {
	return json.Marshal(metar)
}
