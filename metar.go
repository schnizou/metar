package metar

import (
	"fmt"
	"gitlab.com/schnizou/metar/header"
	"strings"
	"time"
)

// Metar holds all the Information that is coming from raw Metar string
type Metar struct {
	CodeType        header.CodeType `json:"code_type" xml:"code_type"`
	ICAO            header.ICAO     `json:"icao" xml:"icao"`
	ObservationTime time.Time       `json:"observation_time" xml:"observation_time"`
}

// ParseRawMetar parses a raw metar string and returns the internal Metar object
func ParseRawMetar(rawMetar string) (Metar, error) {
	// First step: check if string is empty
	var decodedMetar Metar
	if len(rawMetar) == 0 {
		return decodedMetar, fmt.Errorf("metar is empty")
	}

	// Second step: split by whiteSpace
	splitedMetar := strings.Split(rawMetar, " ")

	// Third step: Parse Code Type
	codeType, err := header.ParseCodeType(splitedMetar[0])
	if err != nil {
		return Metar{}, err
	}
	decodedMetar.CodeType = codeType

	// Fourth step: Parse ICAO
	icao, err := header.ParseICAO(splitedMetar[1])
	if err != nil {
		return Metar{}, err
	}
	decodedMetar.ICAO = icao

	return decodedMetar, nil
}
