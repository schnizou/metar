package metar

import (
	"gitlab.com/schnizou/metar/header"
	"reflect"
	"testing"
)

func Test_ParseRawMetar(t *testing.T) {
	type args struct {
		rawMetar string
	}
	tests := []struct {
		name    string
		args    args
		want    Metar
		wantErr bool
	}{
		{"valid METAR", args{"METAR YUDO 221630Z 24004MPS 0800 R12/1000U DZ FG SCT010 OVC020 17/16 Q1018"},
			Metar{CodeType: header.CodeType("METAR"), ICAO: header.ICAO("YUDO")}, false},
		{"valid SPECI", args{"SPECI YUDO 151115Z 05025G37KT 2000 1000S R12/1200N +TSRA BKN005CB 25/22 Q1008"},
			Metar{CodeType: header.CodeType("SPECI"), ICAO: header.ICAO("YUDO")}, false},
		{"valid Type, invalid icao", args{"SPECI YUD 151115Z 05025G37KT 2000 1000S R12/1200N +TSRA BKN005CB 25/22 Q1008"},
			Metar{}, true},
		{"empty should return error", args{""},
			Metar{}, true},
		{"invalid code should return error", args{"pimmel lkjlsad asdasd"},
			Metar{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseRawMetar(tt.args.rawMetar)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseRawMetar() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseRawMetar() got = %v, want %v", got, tt.want)
			}
		})
	}
}
