[![pipeline status](https://gitlab.com/schnizou/metar/badges/main/pipeline.svg)](https://gitlab.com/schnizou/metar/-/commits/main) [![coverage report](https://gitlab.com/schnizou/metar/badges/main/coverage.svg)](https://gitlab.com/schnizou/metar/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/schnizou/metar)](https://goreportcard.com/report/gitlab.com/schnizou/metar)

# Notice
This project is not ready for use.

# Metar

A parser form METAR-Infomation without outcome in machine-readable json,xml etc.

There are generally two very similar, but in some cases different, METAR specifications for International ICOA and ICAO
in Zone K, which is part of the US ICAO. This project will initially address only the specifications for international
ICAO, since these make up the bulk of METAR reports.

# Specification

All specification in this Project are according to the following guidelines

- https://www.dwd.de/DE/leistungen/lf_11_flugwetterbetriebsdienste/handbuch_flugwetterdienst_obs_3.1.pdf?__blob=publicationFile&v=3
- https://mediawiki.ivao.aero/index.php?title=METAR_explanation#Syntax_and_Structure

# Units
We use:

    - The feet (ft) for the clouds height above ground
    - The knot (kt) for the wind speed (kt=nautical mile per hour)
    - The meter (m) for the horizontal visibility
    - The hectopascal (hPa) for the atmospheric pressure (QNH QFE)
    - The degree Celsius (°C) for the temperature measurement

Some unities can change depending of the countries. For example, we use:

    - The meter per second 'mps' (m/s) in Russia for the wind speed
    - The kilometer per hour 'km' (km/h) in Russia for the indicated airspeed
    - The American land mile 'SM' for the visibility in North America
    - The inch of mercury (inHg) for the atmospheric pressure in America (1013hPa=29,92 inHg)
