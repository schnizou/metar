package fields

import (
	"reflect"
	"testing"
)

func Test_parseTemperature(t *testing.T) {
	type args struct {
		temperature string
	}
	tests := []struct {
		name    string
		args    args
		want    Temperature
		wantErr bool
	}{
		{"Valid", args{temperature: "M09/03"}, Temperature{Air: -9, DewPoint: 3}, false},
		{"Valid", args{temperature: "19/10"}, Temperature{Air: 19, DewPoint: 10}, false},
		{"Invalid", args{temperature: "sdfasdkj"}, Temperature{Air: 0, DewPoint: 0}, true},
		{"Invalid", args{temperature: "sdf/asdkj"}, Temperature{Air: 0, DewPoint: 0}, true},
		{"Invalid", args{temperature: "Msdf/asdkj"}, Temperature{Air: 0, DewPoint: 0}, true},
		{"Invalid", args{temperature: "Msdfasdkj"}, Temperature{Air: 0, DewPoint: 0}, true},
		{"Empty", args{temperature: ""}, Temperature{Air: 0, DewPoint: 0}, true},
		{"Invalid", args{temperature: "M10"}, Temperature{Air: 0, DewPoint: 0}, true},
		{"Invalid", args{temperature: "M10/we"}, Temperature{Air: 0, DewPoint: 0}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseTemperature(tt.args.temperature)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseTemperature() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseTemperature() got = %v, want %v", got, tt.want)
			}
		})
	}
}
