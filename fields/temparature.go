package fields

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const temperaturRegex = "^[M]?\\d{2}[/]{1}\\d{2}$"

// Temperature is always available, so there's nothing to omit in json
type Temperature struct {
	Air      int8  `json:"air"`
	DewPoint uint8 `json:"dew_point"`
}

func parseTemperature(temperature string) (result Temperature, err error) {

	validTemperatur := regexp.MustCompile(temperaturRegex)

	if !validTemperatur.MatchString(temperature) {
		return Temperature{}, fmt.Errorf("failed to parse temperature: %s. Invalid format, should be {M}dd\\dd ", temperature)
	}

	sign := int8(1)
	result = Temperature{}

	// if string starts with "M", the temperature will be negative
	if temperature[0:1] == "M" {
		temperature = strings.SplitAfter(temperature, "M")[1]
		sign *= -1
	}

	air, _ := strconv.Atoi(temperature[0:2])
	dewPoint, _ := strconv.Atoi(temperature[3:5])

	result.Air = int8(air) * sign
	result.DewPoint = uint8(dewPoint)

	return result, nil
}
